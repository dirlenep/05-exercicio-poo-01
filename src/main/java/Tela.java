
public class Tela {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Pessoa albert = new Pessoa(14, 3, 1879, "Albert Einstein");
		Pessoa isaac = new Pessoa(4, 1, 1643, "Isaac Newton");
		
		albert.calculaIdade(1, 10, 2021);
		isaac.calculaIdade(1, 10, 2021);
		
		System.out.println(albert.informaNome() + ". " + albert.informaIdade());
		System.out.println(isaac.informaNome() + ". " + isaac.informaIdade());
	}
}
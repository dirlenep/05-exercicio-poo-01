
public class Pessoa {

	private String idade;
	private int diaNascimento;
	private int mesNascimento;
	private int anoNascimento;
	private String nome;
	
	public Pessoa(int diaNascimento, int mesNascimento, int anoNascimento, String nome) {
		setDiaNascimento(diaNascimento);
		setMesNascimento(mesNascimento);
		setAnoNascimento(anoNascimento);
		setNome(nome);
	}
	
	public String getIdade() {
		return idade;
	}
	public void setIdade(String idade) {
		this.idade = idade;
	}
	public int getDiaNascimento() {
		return diaNascimento;
	}
	public void setDiaNascimento(int diaNascimento) {
		this.diaNascimento = diaNascimento;
	}
	public int getMesNascimento() {
		return mesNascimento;
	}
	public void setMesNascimento(int mesNascimento) {
		this.mesNascimento = mesNascimento;
	}
	public int getAnoNascimento() {
		return anoNascimento;
	}
	public void setAnoNascimento(int anoNascimento) {
		this.anoNascimento = anoNascimento;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int calculaIdade(int diaAtual, int mesAtual, int anoAtual) {
		int anoTemp = anoAtual - anoNascimento;
		int mesTemp;
		int diaTemp;
		if (mesAtual > mesNascimento) {
			mesTemp = mesAtual - mesNascimento;
		} else {
			mesTemp = 0;
		}
		if (diaAtual > diaNascimento) {
			diaTemp = diaAtual - diaNascimento;
		} else {
			diaTemp = 0;
		}
		this.idade = "Idade: " + anoTemp + " anos, " + mesTemp + " meses, " + diaTemp + " dias.";
		return anoTemp;
	}
	
	public String informaIdade() {
		return this.idade;
	}
	
	public String informaNome() {
		return this.nome;
	}
	
	public void ajustaDataDeNascimento(int diaNascimento, int mesNascimento, int anoNascimento) {
		this.diaNascimento = diaNascimento;
		this.mesNascimento = mesNascimento;
		this.anoNascimento = anoNascimento;
	}
}

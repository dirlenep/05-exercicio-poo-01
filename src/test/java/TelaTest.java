import static org.junit.Assert.*;

import org.junit.Test;

public class TelaTest {

	@Test
	public void idadeAlbertDeveriaSer142Anos() {
		Pessoa albert = new Pessoa(14, 3, 1879, "Albert Einstein");
		int idade = albert.calculaIdade(1, 10, 2021);
		assertEquals(idade, 142);
	}
	
	@Test
	public void idadeIsaacDeveriaSer378Anos() {
		Pessoa isaac = new Pessoa(4, 1, 1643, "Isaac Newton");
		int idade = isaac.calculaIdade(1, 10, 2021);
		assertEquals(idade, 378);
	}

}
